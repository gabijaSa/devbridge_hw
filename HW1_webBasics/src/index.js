import index from '../public/index.html';
import css from '../public/styles.css';
import TodoApp from './TodoApp.js';

document.addEventListener('DOMContentLoaded', () => {
    const app = new TodoApp();
});