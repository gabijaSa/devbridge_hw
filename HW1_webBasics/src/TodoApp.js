import tasks from '../data/tasks'

class TodoApp {
    constructor() {

        let form = document.querySelector('.js-add-task-form');

        form.addEventListener('submit', (e) => {
            e.preventDefault();
            let taskInput = document.querySelector('.js-add-task-input');

            this.addTask(taskInput)
        });

        this.renderTasks();   
    }

    addTask(taskInput) {
        tasks.push({
            title: taskInput.value,
            completed: false,
        });

        this.renderTasks();
    }

    deleteTask(index) {
        tasks.splice(index, 1);
        this.renderTasks();
    }

    // check if there are any selected items. If yes then delete-multilple-items-button is enabled.
    checkIfSelected(button, selectedItems) {
        if (selectedItems === 0) {
            button.disabled = true;
        }
        else {
            button.disabled = false;
        }
    }
    

    renderTasks() {
        let selectedItems = 0;

        let button = document.getElementById("delete-multilple-items-button");

        // check if there are any tasks. If yes then the delete-multilple-items-button is enabled.
        if (tasks.length === 0) {
            button.disabled = true;
        }
        else {
            button.disabled = false;
        }
        let list = document.querySelector('.js-add-task-list');

        list.innerHTML = '';

        tasks.forEach((task, index) => {
            var newCheckBox = document.createElement('input');
            newCheckBox.type = 'checkbox';
            newCheckBox.style = 'float: left; margin-right: 0.5em';

            // check if a task is completed. If yes then mark the box.
            if (task.completed) {
                newCheckBox.checked = true;
                selectedItems++;
            }
            else {
                newCheckBox.checked = false;
            }
            
            this.checkIfSelected(button, selectedItems); 

            let newTask = document.createElement('div');
            newTask.innerText = task.title;

            let deleteButton = document.createElement('span');
            deleteButton.className = 'glyphicon glyphicon-remove';

            deleteButton.addEventListener('click', () => {
                this.deleteTask(index);
            });

            newCheckBox.addEventListener('change', () => {
                // count selected items
                if (task.completed) {
                    task.completed = false;
                    selectedItems--;
                }
                else {
                    task.completed = true;
                    selectedItems++;
                }

                this.checkIfSelected(button, selectedItems);

            });

            button.addEventListener('click', () => {
                tasks.map((task, index) => {
                    if (task.completed) {
                        this.deleteTask(index);
                    }
                })
            })
            
            newTask.appendChild(newCheckBox);
            newTask.appendChild(deleteButton);
            list.appendChild(newTask); 

        })
    }
}

export default TodoApp;