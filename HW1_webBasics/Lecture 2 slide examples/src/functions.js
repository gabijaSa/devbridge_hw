
function a(b) {   // No type declaration
  return b;
}

function c(fn, d) {    // Function can be used as param
  const temp = [fn];   // Can be stored in an array
  return fn(d);               
}

a = a.bind({}); // Has properties

c(a, 1);

