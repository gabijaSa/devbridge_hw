
// Create <span></span>
const spanElement = document.createElement('span');
// <span class="glyphicon glyphicon-remove"></span
spanElement.className = "glyphicon glyphicon-remove";
// <span class="glyphicon glyphicon-remove">Text in a span</span
spanElement.textContent = 'Text in a span'; 
// Append child to  <body></body>
document.body.appendChild(spanElement);


// Gets the first element with containing class ".glyphicon"
document.querySelector(".glyphicon");

