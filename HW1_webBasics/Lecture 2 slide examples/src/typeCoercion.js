
console.log(null ? true : false);        // false
console.log(undefined ? true : false);   // false
console.log(0 ? true : false);           // false
console.log('' ? true : false)           // false

console.log( 0 == false ? true : false);         // true
console.log( 0 == null ? true : false);          // false
console.log( 0 == '' ? true : false);            // true
console.log( 0 == '1' ? true : false);           // false
console.log( undefined == null ? true : false);  // true

// Use === strict comparison operator to avoid coercion


